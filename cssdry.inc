<?php

/**
 * @file
 *  CSSDRY:ing code from:
 *  http://www.machete.ca/2009/06/more-powerful-css-with-preprocessing/
 */

function _cssdry($raw_css) {
  $cssProcessor = new CSSProcessor($raw_css);
  return $cssProcessor->process();
}

class CSSProcessor {
  private $constants = array();
  private $content;

  public function __construct($content) {
    $this->content = $content;
  }

  /**
   * Processes CSS so that:
   * 
   * 1. constants may be defined.  For example $background=#000
   *      would allow for the use of $background instead of the hard coded #000  
   * 2. Nesting of CSS rules.
   *     For example #navbar a { ... } could be rewritten as #navbar { a{ ... } }
   * 3. Single line comments are allowed ala //this is a comment style
   * 4. The following are stripped out: comments, unnecessary whitespace, empty rules (a {})
   */
  public function process() {
    $this->expandConstants();
    $this->fixNesting();
    $this->fixMedia();
    $this->minify();
    
    return $this->content;
  }

  private function expandConstants() {
    $css = preg_replace_callback(
      '/\$([a-z_-]+)+=([^;]+);?/',
      array($this, 'extractConstantsCallback'),
      $this->content);

    $css = preg_replace_callback(
      '/\$([a-z_-]+)/',
      array($this, 'substituteConstantsCallback'),
      $css);

    $this->content = $css;
  }

  private function extractConstantsCallback(array $matches) {
    $constant = $matches[1];
    $value = $matches[2];
    $this->constants[$constant] = $value;
    return '';
  }

  private function substituteConstantsCallback(array $matches) {
    $constant_name = $matches[1];

    $value = '';
    if (!isset($this->constants[$constant_name])) {
      $value = "/* Unknown constant {$constant_name} */";
    }
    else {
      $value = $this->constants[$constant_name];
    }

    return $value;
  }
  
  /**
   * Nested @media blocks in a css file should remain nested and not broken out.
   */
  private function fixMedia() {
    $this->content = preg_replace_callback('#@media [^\{]+ \{\}#', '_cssdry_fix_media_callback', $this->content);
  }
  
  private function fixNesting() {
    $result = array();
    $pieces = split("{", $this->content);

    $selectorStackIndex = 0;
    $selectorStack = array();

    $currentPieceIndex = 0;

    for ($i = 0; $i < count($pieces); $i++) {
      $piece = $pieces[$i];
      
      while (($closeBracketPos = strpos($piece, "}")) !== FALSE) {
        if ($closeBracketPos > 0) {
          $result[] = substr($piece, 0, $closeBracketPos);
        }

        $result[] = "}";

        $selectorStackIndex--;

        if ($selectorStackIndex > 0 ) {
          $result[] = $this->fixNestingSelector($selectorStack, $selectorStackIndex);
          $result[] = "{";
        }
        $piece = substr($piece, $closeBracketPos+1);
      }

      if (trim($piece) === '')
        continue;

      // Inner Rule
      $endOfLastProperty = strrpos($piece, ";");
      if ($endOfLastProperty !== false) {
        $result[] = substr($piece, 0, $endOfLastProperty+1);
        $piece = substr($piece, $endOfLastProperty+1);
      }

      if ($selectorStackIndex > 0)
        $result[] = "}";

      // Whole piece is the selector
      $selectorStack[$selectorStackIndex++] = $piece;
      
      $result[] = $this->fixNestingSelector($selectorStack, $selectorStackIndex);
      $result[] = '{';
      
    }

    $this->content = join($result);
  }

  private function fixNestingSelector($selectors, $depth) {
    if (strpos($selectors[0], '@media') === FALSE || $depth === 1) {
      $newSelector = $selectors[0];
    
      for($j = 1; $j<$depth; $j++) {
        $newSelector = $this->fixNestingBuildSelector($newSelector, $selectors[$j]);
      }
    } else {
      $newSelector = $selectors[1]; 
      for($j = 2; $j<$depth; $j++) {
        $newSelector = $this->fixNestingBuildSelector($newSelector, $selectors[$j]);
      }
    }
    
    // Allow pseudo classes to be defined nested, like the following:
    // a { 
    //   color: red; 
    //
    //   :hover { 
    //     color: blue;
    //   }
    // }
    $newSelector = preg_replace('# (:[a-z\-]+)#', '$1', $newSelector);
    
    return $newSelector;
  }

  private function fixNestingBuildSelector($outer, $inner) {
    $outerPieces = split(",", $outer);
    $innerPieces = split(",", $inner);

    $resultPieces = array();
    foreach ($outerPieces as $o) {
      foreach ($innerPieces as $i) {
        $resultPieces[] = trim($o) . " " . trim($i);
      }
    }

    return join(",", $resultPieces);
  }
  
  /**
   * Minify the resulting CSS to remove ugly empty selectors. Although when css
   * aggregation is enabled this isn't necessary most of development is done when it isn't.
   */
  private function minify() {
    // remove single line comments, like this, from // to \\n
    $data = preg_replace('/(\/\/.*\n)/', '', $this->content);

    // remove new lines \\n, tabs and \\r
    $data = preg_replace('/(\t|\r|\n)/', '', $data);

    // remove multi-line comments
    $data = preg_replace('/(\/\*[^*]*\*\/)/', '', $data);

    // replace multi spaces with singles
    $data = preg_replace('/(\s+)/', ' ', $data);

    //Remove empty rules
    $data = preg_replace('/[^}{]+{\s?}/', '', $data);

    // Remove whitespace around selectors and braces
    $data = preg_replace('/\s*{\s*/', '{', $data);

    // Remove whitespace at end of rule
    $data = preg_replace('/\s*}\s*/', '}', $data);

    // Just for clarity, make every rule 1 line tall
    $data = preg_replace('/}/', "}\n", $data);

    $this->content = $data;
  }
}

/**
 * Alternates between either the start of a media block or the end of one.
 */
function _cssdry_fix_media_callback($parts) {
  static $call_count = 0;
  $call_count ++;
  if ($call_count % 2 === 1) {
    return substr($parts[0], 0, strlen($parts[0]) - 1);
  } else {
    return '}'; 
  }
}